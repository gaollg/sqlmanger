function load(json){
	if(json.code!=0){
		alert(json.msg);
		return ;
	}
	var p = $('#database').empty();
	$('#database-tmpl').tmpl(json).appendTo(p);
}

var reload = function(){
	$.ajax({
		'async' : true,
		'type' : "post",
		'url' : 'list.json',
		'data' : {},
		'dataType' : 'json',
		'success' : function(json){
			load(json);
		}
	});
};

$(function(){
	$('#creatDatabase').click(function(){
		var a = $('<div>数据库名称:<input type="text" data-form="database" value="" class="input-xlarge" placeholder="请输入数据库名称"></div>' +
				'<div>创建数据库理由:<input type="text" value="" data-form="reason" class="input-xlarge" placeholder="创建数据库理由"></div>' );
		$.g.dialg({
			title : "申请创建数据库", 
			content: a
		}, function(v, d){
			var name = a.find('[data-form=database]').val().trim();
			if(!name){
				alert('不允许为空');
				return ;
			}
			var reason = a.find('[data-form=reason]').val().trim();
			$.post('addDatabase.json', {name : name, reason :reason}, function(json){
				d.close();
				reload();
			}, 'json');
		});
		
	});
	
	//申请
	$('#database [data-e=applyMsg]').live('click', function(){
		var $this = $(this);
		var id = $this.parents('[data-id]').attr('data-id');
		//查询未处理的申请
		$.g.dialg({
			content: "正在加载数据..."
		}, function(d){
			d.removeClass('small');
			d.find('.modal-header').remove();
			$.ajax({
				'async' : true,
				'type' : "post",
				'url' : 'applyInfo.json',
				'data' : {id: id},
				'dataType' : 'json',
				'success' : function(json){
					var body = d.find('[data-d-who=body]');
					body.empty();
					var a = $('#applyInfo-tmpl').tmpl(json.data);
					body.append(a);
				}
			});
			d.on('click', '[data-type]', function(){
				var $this = $(this);
				var msg = $this.attr('data-type');
				var id = $this.parent().attr('data-id');
				var o = {
					"SUCCESS" : "通过",
					"FAIL" : "拒绝"
				};
				if(confirm('确定'+o[msg]+"吗？")){
					$.ajax({
						'async' : true,
						'type' : "post",
						'url' : '../databasemanger/operate.json',
						'data' : {id: id, type:msg},
						'dataType' : 'json',
						'success' : function(json){
							if(json.code!=0){
								alert(json.msg);
								return ;
							}
							d.close();
							reload();
						}
					});
				}
			});
		}, function(v, d){
			d.close();
		});
	});
	
	//操作 
	$('#database [data-e-operate]').live('click', function(){
		var $this = $(this);
		var id = $this.parents('[data-id]').attr('data-id');
		var operate = $this.attr('data-e-operate');
		var a = $('<div>'+"确定申请[" + getOperate(operate) + "]数据库?"+
				'<input type="text" data-form="reason" value="" class="input-xlarge" placeholder="请输入理由"></div>');
		
		$.g.dialg({
			title : "申请确认",
			content: a
		}, function(v, d){
			var reason = a.find('[data-form=reason]').val().trim();
			if(!reason){
				alert('不允许为空');
				return ;
			}
			
			$.ajax({
				'async' : true,
				'type' : "post",
				'url' : 'applyOperate.json',
				'data' : {id:id, operate:operate,reason:reason},
				'dataType' : 'json',
				'success' : function(json){
					if(json.code!=0){
						alert(json.msg);
						return ;
					}
					reload();
					d.close();
				}
			});
		});
	});
});
