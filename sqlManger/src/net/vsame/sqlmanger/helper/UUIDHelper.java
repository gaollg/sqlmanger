package net.vsame.sqlmanger.helper;

import java.util.UUID;

public class UUIDHelper {

	public static String uuid(){
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	public static void main(String[] args) {
		System.out.println(uuid());
	}
	
}
