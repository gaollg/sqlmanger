package net.vsame.sqlmanger.helper;

import java.util.Date;
import java.util.Map;

import net.vsame.url2sql.domain.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.utils.JdbcUtils;

public class UserHelper {
	
	private static final String USER = "USER";

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getUser(){
		Url2SqlContext c = WebHelper.getContext();
		Object o = c.getSession().getAttribute(USER);
		if(o == null){
			return null;
		}
		return (Map<String, Object>) o;
	}
	
	public static Long getUserId(){
		Map<String, Object> map = getUser();
		if(map!=null){
			Object o = map.get("id");
			if(o instanceof Long){
				return (Long) o;
			}
			return Long.parseLong(o+"");
		}
		return 5L;
	}
	
	public static void main(String[] args) {
		WebHelper.init(null, null);
		String sql = "INSERT INTO `database_apply_t` (`databaseId`, `operate`, `reason`, `applyId`, `status`, createTime) VALUES (13, 'CREAT', ?, 5, 'APPLY', ?)";
		JdbcUtils.execute(sql, "中", new Date());
		WebHelper.remove();
	}

}
