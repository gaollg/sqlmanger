/*
Navicat MySQL Data Transfer

Source Server         : 10.0.2.2_3306
Source Server Version : 50163
Source Host           : 10.0.2.2:3306
Source Database       : mysql_manger

Target Server Type    : MYSQL
Target Server Version : 50163
File Encoding         : 65001

Date: 2013-08-01 17:35:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `database_apply_t`
-- ----------------------------
DROP TABLE IF EXISTS `database_apply_t`;
CREATE TABLE `database_apply_t` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `databaseId` int(4) DEFAULT NULL,
  `applyId` int(11) DEFAULT NULL,
  `operate` varchar(10) DEFAULT NULL,
  `apply` longtext COMMENT '申请期望结果',
  `reason` varchar(500) DEFAULT NULL,
  `handlerId` int(4) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of database_apply_t
-- ----------------------------
INSERT INTO `database_apply_t` VALUES ('1', '1', null, 'ALTER', '{}', 'xxxxx', '2', 'APPLY');
INSERT INTO `database_apply_t` VALUES ('2', '1', null, 'ALTER', 'sss', 'sss', '5', null);
INSERT INTO `database_apply_t` VALUES ('3', '2', null, null, null, null, null, 'APPLY');
INSERT INTO `database_apply_t` VALUES ('4', '2', null, null, null, null, null, null);
INSERT INTO `database_apply_t` VALUES ('5', '3', null, 'CREAT', null, '没事干', '2', 'APPLY');
INSERT INTO `database_apply_t` VALUES ('6', '8', null, 'CREAT', null, 'essssssss', '2', 'APPLY');

-- ----------------------------
-- Table structure for `database_t`
-- ----------------------------
DROP TABLE IF EXISTS `database_t`;
CREATE TABLE `database_t` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `databaseName` varchar(50) DEFAULT NULL,
  `applyTime` datetime DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `removed` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of database_t
-- ----------------------------
INSERT INTO `database_t` VALUES ('1', 'mysssss', null, '2013-07-31 17:11:31', 'N');
INSERT INTO `database_t` VALUES ('2', 'xxxx', null, null, 'N');
INSERT INTO `database_t` VALUES ('5', 'dfsdfsd', null, null, 'N');
INSERT INTO `database_t` VALUES ('6', 'sdfsdfsdf', null, null, 'N');
INSERT INTO `database_t` VALUES ('7', 'eeeeeeeee', null, null, 'N');
INSERT INTO `database_t` VALUES ('8', 'eee', null, null, 'N');

-- ----------------------------
-- Table structure for `member_t`
-- ----------------------------
DROP TABLE IF EXISTS `member_t`;
CREATE TABLE `member_t` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_t
-- ----------------------------

-- ----------------------------
-- Table structure for `table_apply_t`
-- ----------------------------
DROP TABLE IF EXISTS `table_apply_t`;
CREATE TABLE `table_apply_t` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tableId` int(4) DEFAULT NULL,
  `operate` varchar(10) DEFAULT NULL,
  `apply` longtext COMMENT '申请期望结果',
  `reason` varchar(500) DEFAULT NULL,
  `handlerId` int(4) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of table_apply_t
-- ----------------------------
INSERT INTO `table_apply_t` VALUES ('1', '1', 'ALTER', 'aaaa', '想了', null, null);
INSERT INTO `table_apply_t` VALUES ('2', '1', 'ALTER', 'ALTER', null, null, null);

-- ----------------------------
-- Table structure for `table_t`
-- ----------------------------
DROP TABLE IF EXISTS `table_t`;
CREATE TABLE `table_t` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(50) DEFAULT NULL,
  `removed` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of table_t
-- ----------------------------
